import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID 93ZqIbdzm5PHwpRf6gOIRMSgcXIdMTZQnrKWbjXWRyI'
    }
});