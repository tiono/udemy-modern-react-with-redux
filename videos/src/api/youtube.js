import axios from 'axios';

const KEY = 'AIzaSyBoiM7t3qa15z7BBevbjYQN9M-Y84gmJ6A';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResults: 5,
        key: KEY,
        type: 'video'
    }
});